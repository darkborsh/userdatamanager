package com.exercise.dao;

import com.exercise.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<User, Long>, CustomUserDao {
    User getById(long id);
}
