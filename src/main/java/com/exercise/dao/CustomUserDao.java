package com.exercise.dao;

import com.exercise.model.User;
import com.exercise.model.UserStatus;

import java.util.List;

public interface CustomUserDao {
    List<User> findAllByStatusAndTime(UserStatus userStatus, Long unixTimeId);
}
