package com.exercise.dao;

import com.exercise.model.User;
import com.exercise.model.UserStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Repository
public class CustomUserDaoImpl implements CustomUserDao {
    private final EntityManager entityManager;

    @Override
    public List<User> findAllByStatusAndTime(UserStatus userStatus, Long unixTimeId) {
        boolean appendFlag = false;
        StringBuilder jpql = new StringBuilder("from User u");
        if (userStatus != UserStatus.ANY) {
            jpql
                    .append(" where u.userStatus = ")
                    .append("\'" + userStatus.toString() + "\'");
            appendFlag = true;
        }
        if (unixTimeId != null) {
            if (appendFlag) {
                jpql
                        .append(" and u.lastTimeStatusChanged > ")
                        .append(new Date(unixTimeId));
            } else {
                jpql
                        .append(" where u.lastTimeStatusChanged > ")
                        .append("\'" + new Timestamp(new Date(unixTimeId).getTime()) + "\'");
            }
        }
        log.error(jpql.toString());
        TypedQuery<User> typedQuery = entityManager.createQuery(jpql.toString(), User.class);
        return typedQuery.getResultList();
    }
}
