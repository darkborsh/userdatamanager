package com.exercise.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChangeStatusRequest {
    long uuid;
    UserStatus newStatus;
    UserStatus oldStatus;
}
