package com.exercise.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@Proxy(lazy = false)
@Table(name = "users")
@Entity
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @NotBlank
    private String name;

    @Column(name = "avatar_uri")
    @NotBlank
    private String avatarURI;

    @NotBlank
    private String email;

    @NotBlank
    private String sex;

    @Column(name = "birthdate")
    @NotNull
    private Date birthDate;

    private int age;

    @NotNull
    private Date lastTimeStatusChanged;

    @Column(name = "user_status")
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;
}
