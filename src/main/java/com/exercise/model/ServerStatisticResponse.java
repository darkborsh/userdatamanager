package com.exercise.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class ServerStatisticResponse {
    List<User> userList = new ArrayList<>();
    Long unixTimeId;
}
