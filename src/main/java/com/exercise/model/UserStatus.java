package com.exercise.model;

public enum UserStatus {
    OFFLINE,
    ONLINE,
    ANY
}
