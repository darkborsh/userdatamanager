package com.exercise.controller;

import com.exercise.model.ChangeStatusRequest;
import com.exercise.model.ServerStatisticResponse;
import com.exercise.model.User;
import com.exercise.model.UserStatus;
import com.exercise.service.*;
import com.exercise.task.*;
import com.exercise.task.impl.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;
    private final TaskService taskService;
    private final FileService fileService;

    @GetMapping
    public ResponseEntity<ServerStatisticResponse> getAllUsers(@NotNull @RequestParam (name = "userStatus") UserStatus userStatus,
                                                               @RequestParam (name = "unixTimeId", required = false) Long unixTimeId)
            throws ExecutionException, InterruptedException {
        Task<?> t = new GetServerStatisticsTask(userStatus, unixTimeId, userService);
        taskService.executeTask(t).get();
        return ResponseEntity.ok((ServerStatisticResponse) t.getResult());
    }

    @PostMapping
    public ResponseEntity<Map<String, Long>> saveUser(@RequestBody User user)
            throws ExecutionException, InterruptedException {
        Task<?> t = new CreateUserTask(user, userService);
        if (user != null) {
            taskService.executeTask(t).get();
            return ResponseEntity.ok(new HashMap<>() {{
                put("uuid", (Long) t.getResult());
            }});
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/avatars")
    ResponseEntity<Map<String, String>> uploadAvatar(@RequestParam("file") MultipartFile file)
            throws ExecutionException, InterruptedException {
        Task<?> t = new UploadFileTask(file, fileService);
        taskService.executeTask(t).get();
        String result = (String) t.getResult();
        if (result != null) {
            return ResponseEntity.ok(new HashMap<>() {{
                put("filePath", result);
            }});
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") @Min(1) long id)
            throws ExecutionException, InterruptedException {
        Task<?> t = new GetUserByIdTask(id, userService);
        taskService.executeTask(t).get();
        User user = (User) t.getResult();
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ChangeStatusRequest> toggleUserStatus(@PathVariable("id") @Min(1) long id,
                                                                @NotNull @RequestParam (name = "userStatus") UserStatus userStatus)
            throws ExecutionException, InterruptedException {
        Task<?> t = new ToggleUserStatusByIdTask(id, userStatus, userService);
        taskService.executeTask(t).get();
        ChangeStatusRequest req = (ChangeStatusRequest) t.getResult();
        if (req != null) {
            return ResponseEntity.ok(req);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
