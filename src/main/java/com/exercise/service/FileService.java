package com.exercise.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Slf4j
@AllArgsConstructor
@Service
public class FileService {
    public String storeFile(MultipartFile file) throws IOException {
        String rootPath = System.getProperty("user.dir");
        String newFilePath = rootPath + "/src/main/resources/" + System.currentTimeMillis() + ".jpg";
        File storageFile = new File(newFilePath);
        file.transferTo(storageFile);
        return newFilePath;
    }
}
