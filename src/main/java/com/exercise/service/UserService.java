package com.exercise.service;

import com.exercise.dao.UserDao;
import com.exercise.model.ChangeStatusRequest;
import com.exercise.model.User;
import com.exercise.model.UserStatus;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
public class UserService {
    private final UserDao userDao;

    public Long saveUser(User user) {
        if (user.getUserStatus() == null) user.setUserStatus(UserStatus.ONLINE);
        user.setLastTimeStatusChanged(new Date());
        return userDao.save(user).getId();
    }

    public List<User> getAllUsers(UserStatus userStatus, Long unixTimeId) {
        return userDao.findAllByStatusAndTime(userStatus, unixTimeId);
    }

    public Optional<User> getUserById(long id) {
        if (userDao.existsById(id)) {
            return Optional.of(userDao.getById(id));
        } else {
            return Optional.empty();
        }
    }

    public ChangeStatusRequest toggleUserStatus(long id, UserStatus userStatus) {
        if (userDao.existsById(id)) {
            User user = Optional.of(userDao.getById(id)).get();

            ChangeStatusRequest response = new ChangeStatusRequest();
            response.setUuid(id);
            response.setOldStatus(user.getUserStatus());
            response.setNewStatus(userStatus);

            user.setUserStatus(userStatus);
            user.setLastTimeStatusChanged(new Date());

            userDao.save(user);

            return response;
        } else {
            return null;
        }
    }
}
