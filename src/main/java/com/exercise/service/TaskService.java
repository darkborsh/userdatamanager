package com.exercise.service;

import com.exercise.task.Task;
import com.exercise.task.TaskPriority;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

@AllArgsConstructor
@Service
public class TaskService {
    private ThreadPoolExecutor threadPoolExecutor;

    public <T> Future<Task<T>> executeTask(Task<T> task) {
        if (task.getPriority() == TaskPriority.HIGH) {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        } else if (task.getPriority() == TaskPriority.LOW) {
            Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
        }
        return (Future<Task<T>>) threadPoolExecutor.submit(task);
    }
}
