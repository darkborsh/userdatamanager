package com.exercise.config;

import com.exercise.task.Task;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.*;

@Slf4j
@Configuration
@Getter
public class MultiThreadingConfig {
    @Bean
    public ThreadPoolExecutor threadPoolExecutor(){
        return new MultiThreadingConfig.CustomSingleThreadPoolExecutor(new PriorityBlockingQueue<>());
    }

    class CustomSingleThreadPoolExecutor extends ThreadPoolExecutor{
        public CustomSingleThreadPoolExecutor(BlockingQueue<Runnable> workQueue) {
            super(1, 1, 1000, TimeUnit.SECONDS, workQueue);
        }

        @Override
        protected <T> RunnableFuture<T> newTaskFor(Runnable runnable, T value) {
            return new CustomFutureTask<>(runnable);
        }
    }

    class CustomFutureTask<T> extends FutureTask<T> implements Comparable<CustomFutureTask<T>> {
        private final Task task;

        public CustomFutureTask(Runnable task) {
            super(task, null);
            this.task = (Task) task;
        }

        @Override
        public int compareTo(CustomFutureTask that) {
            return this.task.getPriority().compareTo(that.task.getPriority());
        }
    }
}
