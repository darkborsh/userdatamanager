package com.exercise.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@NoArgsConstructor
@AllArgsConstructor
@Getter
public abstract class Task<V> implements Runnable {
    private static final long TIMEOUT = 0;
    protected volatile V result;
    protected TaskPriority priority;

    public abstract void execute();

    @Override
    public void run()
    {
        try {
            log.error("Обработка задачи с приоритетом: {}", this.priority);
            Thread.sleep(TIMEOUT);
            this.execute();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
