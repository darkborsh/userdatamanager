package com.exercise.task.impl;

import com.exercise.model.ServerStatisticResponse;
import com.exercise.model.UserStatus;
import com.exercise.task.TaskPriority;
import com.exercise.service.UserService;
import com.exercise.task.Task;

import java.util.Date;

public class GetServerStatisticsTask extends Task<ServerStatisticResponse> {
    private final Long unixTimeId;
    private final UserStatus userStatus;
    private final UserService userService;

    public GetServerStatisticsTask(UserStatus userStatus, Long unixTimeId, UserService userService) {
        this.unixTimeId = unixTimeId;
        this.userStatus = userStatus;
        this.userService = userService;
        this.priority = TaskPriority.HIGH;
    }

    @Override
    public void execute() {
        ServerStatisticResponse statistics = new ServerStatisticResponse();
        statistics.setUserList(userService.getAllUsers(userStatus, unixTimeId));
        statistics.setUnixTimeId(new Date().getTime());
        result = statistics;
    }

    @Override
    public String toString() {
        return "Task: GetServerStatistics, Priority: HIGH";
    }
}
