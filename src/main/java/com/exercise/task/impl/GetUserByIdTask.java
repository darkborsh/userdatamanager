package com.exercise.task.impl;

import com.exercise.model.User;
import com.exercise.task.TaskPriority;
import com.exercise.service.UserService;
import com.exercise.task.Task;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Slf4j
public class GetUserByIdTask extends Task<User> {
    private final UserService userService;
    private final long id;

    public GetUserByIdTask(long id, UserService userService) {
        this.id = id;
        this.userService = userService;
        this.priority = TaskPriority.HIGH;
    }

    @Override
    public void execute() {
        try {
            Optional<User> user = userService.getUserById(id);
            this.result = user.orElse(null);
        } catch (EntityNotFoundException e) {
            log.error("Not found");
            this.result = null;
        }
    }
}
