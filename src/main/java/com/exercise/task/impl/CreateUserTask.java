package com.exercise.task.impl;

import com.exercise.model.User;
import com.exercise.task.TaskPriority;
import com.exercise.service.UserService;
import com.exercise.task.Task;

public class CreateUserTask extends Task<Long> {
    private final User user;
    private final UserService userService;

    public CreateUserTask(User user, UserService userService) {
        this.user = user;
        this.userService = userService;
        this.priority = TaskPriority.LOW;
    }

    @Override
    public void execute() {
        if (user != null) {
            this.result = userService.saveUser(user);
        }
    }

    @Override
    public String toString() {
        return "Task: createUser, Priority: LOW";
    }
}
