package com.exercise.task.impl;

import com.exercise.model.ChangeStatusRequest;
import com.exercise.model.UserStatus;
import com.exercise.task.TaskPriority;
import com.exercise.service.UserService;
import com.exercise.task.Task;

import javax.persistence.EntityNotFoundException;

public class ToggleUserStatusByIdTask extends Task<ChangeStatusRequest> {
    private final long id;
    private final UserStatus userStatus;
    private final UserService userService;

    public ToggleUserStatusByIdTask(long id, UserStatus userStatus, UserService userService) {
        this.id = id;
        this.userStatus = userStatus;
        this.userService = userService;
        this.priority = TaskPriority.LOW;
    }

    @Override
    public void execute() {
        try {
            this.result = userService.toggleUserStatus(id, userStatus);
        } catch (EntityNotFoundException e) {
            this.result = null;
        }
    }
}
