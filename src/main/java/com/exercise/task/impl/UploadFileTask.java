package com.exercise.task.impl;

import com.exercise.service.FileService;
import com.exercise.task.TaskPriority;
import com.exercise.task.Task;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
public class UploadFileTask extends Task<String> {
    private final MultipartFile file;
    private final FileService fileService;

    public UploadFileTask(MultipartFile file, FileService fileService) {
        this.file = file;
        this.fileService = fileService;
        this.priority = TaskPriority.LOW;
    }

    @Override
    public void execute() {
        try {
            result = fileService.storeFile(file);
        } catch (IOException e) {
            log.error("Catch exception while writing the file");
            e.printStackTrace();
        }
    }
}
