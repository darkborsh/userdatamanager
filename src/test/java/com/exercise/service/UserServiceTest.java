package com.exercise.service;

import com.exercise.dao.UserDao;
import com.exercise.model.ChangeStatusRequest;
import com.exercise.model.User;
import com.exercise.model.UserStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class UserServiceTest {
    private UserDao userDao;
    private UserService userService;

    @BeforeEach
    public void setup() {
        userDao = mock(UserDao.class);
        userService = new UserService(userDao);
    }

    @Test
    public void getAllUsers_DoNotReturnNull_Always() {
        when(userDao.findAllByStatusAndTime(any(UserStatus.class), anyLong())).thenReturn(Collections.emptyList());
        when(userService.getAllUsers(any(UserStatus.class), anyLong())).thenReturn(Collections.emptyList());

        List<User> userListWithoutNullParam = userService.getAllUsers(UserStatus.ANY, 1233L);
        List<User> userListWithNullParam = userService.getAllUsers(UserStatus.OFFLINE, null);


        assertNotNull(userListWithoutNullParam);
        assertNotNull(userListWithNullParam);
    }

    @Test
    public void getUserById_ReturnsUser_IfUserExist() {
        when(userDao.existsById(eq(1L))).thenReturn(true);
        when(userDao.getById(1L)).thenReturn(new User());

        User user = userService.getUserById(1L).get();

        assertNotNull(user);
    }

    @Test
    public void getUserById_ReturnsNull_IfUserNotFound() {
        when(userDao.existsById(anyLong())).thenReturn(false);
        when(userDao.getById(1L)).thenReturn(null);

        Optional<User> user = userService.getUserById(1L);

        assertEquals(Optional.empty(), user);
    }

    @Test
    public void toggleUserStatus_ReturnsNull_IfUserNotFound() {
        when(userDao.existsById(anyLong())).thenReturn(false);
        when(userDao.getById(1L)).thenReturn(null);

        ChangeStatusRequest changeStatusRequest = userService.toggleUserStatus(1L, any(UserStatus.class));

        assertNull(changeStatusRequest);
    }

    @Test
    public void toggleUserStatus_ChangeStatus_IfUserExist() {
        User user = new User();
        user.setUserStatus(UserStatus.ANY);
        when(userDao.existsById(anyLong())).thenReturn(true);
        when(userDao.getById(eq(1L))).thenReturn(user);
        when(userDao.save(any())).thenReturn(user);

        userService.toggleUserStatus(1L, UserStatus.ONLINE);

        verify(userDao).save(any());
    }
}
